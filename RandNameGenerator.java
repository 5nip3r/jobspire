import java.util.Random;
import java.util.Scanner;

/**
 * RandNamegenerator servers two purposes: 1. takes a variable name as parameter
 * 2. Generate a random number between 6 and 15 and return the result in this
 * format: “Name GeneratedNumber”. Eg: “Rahul 11”
 * 
 * @author <a href="mailto:mail2gahlot@gmail.com">Jitender Singh Gahlot</a>
 */
public class RandNameGenerator {

	/**
	 * @param name
	 * @return name
	 */
	public String readName(String name) {
		Random r = new Random();
		int lowLimit = 6; // inclusive of 6 
		int highLimit = 16; // exclusive of 16
		int result = r.nextInt(highLimit - lowLimit) + lowLimit;
		return "Name GeneratedNumber: " + " " + name + " " + result;
	}

	public static void main(String... args) {
		RandNameGenerator randNameGenerator = new RandNameGenerator(); //Create object of this class.
		System.out.println("Enter Name");
		Scanner scan = new Scanner(System.in); // Take input from console.
		String name = scan.next();
		System.out.println(randNameGenerator.readName(name)); // Print the formatted text over console.

	}

}